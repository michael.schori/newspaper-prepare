from django.forms import ModelForm
from .models import Category, Article


class CategoryForm(ModelForm):
    """
    Form: Model <Category>
    Form-Class for Category-Model.
    """

    class Meta:
        model = Category
        fields = ['name', 'description']


class ArticleForm(ModelForm):
    """
    Form: Model <Article>
    Form-Class for Article-Model.
    """

    class Meta:
        model = Article
        fields = ['url_param', 'title', 'content', 'category', 'user']

    def __init__(self, *args, **kwargs):
        super(ArticleForm, self).__init__(*args, **kwargs)
        self.fields['category'].empty_label = None
        self.fields['user'].empty_label = None
