from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    """
    Model: Category
    Store categories for news-articles.
    """
    name = models.CharField(max_length=50, null=False)
    description = models.TextField(null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name}'


class Article(models.Model):
    """
    Model: Article
    Store news-articles.
    """
    url_param = models.CharField(max_length=50, null=False)
    title = models.CharField(max_length=50, null=False)
    content = models.TextField(null=False)
    category = models.ForeignKey(Category, on_delete=models.RESTRICT)
    user = models.ForeignKey(User, on_delete=models.RESTRICT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.title}'
