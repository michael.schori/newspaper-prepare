from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Category, Article
from .forms import CategoryForm, ArticleForm


def list_categories(request):
    """
    Views to list all categories.
    :param request: request from user
    :return: rendered RD_category.html
    """
    categories = Category.objects.all()
    return render(request, 'news/RD_category.html', {'categories': categories})


def create_category(request):
    """
    View to create new category.
    :param request: request from user
    :return: rendered CU_category.html or redirect to list_categories
    """
    form = CategoryForm()
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Created category successfully!')
            return redirect('list_categories')
    return render(request, 'news/CU_category.html', {'form': form, 'btn_txt': 'Create new category'})


def update_category(request, category_id):
    """
    View to update a category.
    :param request: request from user
    :param category_id: category-id
    :return: rendered CU_category.html or redirect to list_categories
    """
    try:
        category = Category.objects.get(pk=category_id)
        form = CategoryForm(instance=category)
        if request.method == 'POST':
            form = CategoryForm(request.POST, instance=category)
            if form.is_valid():
                form.save()
                messages.success(request, 'Updated category successfully!')
                return redirect('list_categories')
    except Category.DoesNotExist:
        messages.error(request, 'No category with this id found!')
        return redirect('list_categories')
    return render(request, 'news/CU_category.html', {'form': form, 'btn_txt': 'Update category'})


def delete_category(request, category_id):
    """
    View to delete a category.
    :param request: request from user
    :param category_id: category-id
    :return: redirect to list_categories
    """
    try:
        category = Category.objects.get(pk=category_id)
        category.delete()
        messages.info(request, f'Deleted category {category.name}')
    except Category.DoesNotExist:
        messages.error(request, 'No category with this id found!')
    return redirect('list_categories')


def list_articles(request):
    """
    View to list articles.
    :param request: request from user
    :return: rendered RD_article.html
    """
    articles = Article.objects.all()
    return render(request, 'news/RD_article.html', {'articles': articles})


def create_article(request):
    """
    View to create new article.
    :param request: request from user
    :return: rendered CU_article.html or redirect to list_articles
    """
    form = ArticleForm()
    if request.method == 'POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Created article successfully!')
            return redirect('list_articles')
    return render(request, 'news/CU_article.html', {'form': form, 'btn_txt': 'Create article'})


def update_article(request, article_id):
    """
    View to update an article.
    :param request: request from user
    :param article_id: article-id
    :return: rendered CU_article.html or redirect to list_articles
    """
    try:
        article = Article.objects.get(pk=article_id)
        form = ArticleForm(instance=article)
        if request.method == 'POST':
            form = ArticleForm(request.POST, instance=article)
            if form.is_valid():
                form.save()
                messages.success(request, 'Updated article successfully!')
                return redirect('list_articles')
    except Article.DoesNotExist:
        messages.error(request, 'No article with this id found!')
        return redirect('list_articles')
    return render(request, 'news/CU_article.html', {'form': form, 'btn_txt': 'Update article'})


def delete_article(request, article_id):
    """
    View to delete an article.
    :param request: request from user
    :param article_id: article-id
    :return: redirect to list_articles
    """
    try:
        article = Article.objects.get(pk=article_id)
        article.delete()
        messages.info(request, f'Deleted article <{article.title}>')
    except Article.DoesNotExist:
        messages.error(request, 'No article with this id found!')
    return redirect('list_articles')
