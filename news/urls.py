from django.urls import path
from . import views

urlpatterns = [
    path('categories', views.list_categories, name='list_categories'),
    path('category', views.create_category, name='create_category'),
    path('category/<int:category_id>', views.update_category, name='update_category'),
    path('category/<int:category_id>/delete', views.delete_category, name='delete_category'),
    path('articles', views.list_articles, name='list_articles'),
    path('article', views.create_article, name='create_article'),
    path('article/<int:article_id>', views.update_article, name='update_article'),
    path('article/<int:article_id>/delete', views.delete_article, name='delete_article')
]
